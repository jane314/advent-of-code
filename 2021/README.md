## To run the solutions

- Install Go: https://go.dev/.
- Download `input.txt` to `advent-of-code/2021`, and change directory to `advent-of-code/2021`.
- Run `go run 01a.go`, replacing `01a.go` as appropriate.
