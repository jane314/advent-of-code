package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {

	var a, b, c, sum_cur, sum_prev, index, count int64

	// Open file
	file, err := os.Open("./input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Read file
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		a = b
		b = c
		c, err = strconv.ParseInt(scanner.Text(), 10, 64)
		sum_prev = sum_cur
		sum_cur = a + b + c
		if err != nil {
			log.Fatal(err)
		}
		if index > 2 && sum_cur > sum_prev {
			count++
		}
		index++
	}

	fmt.Println(count)
}
