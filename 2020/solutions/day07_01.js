
let eq = (a, b) => JSON.stringify(a) == JSON.stringify(b);

let has = (arr, elt) => arr.indexOf(elt) != -1;

let input = Deno.readTextFileSync('output.txt').split('\n').filter( str => str.length != 0 )
	.map( str => str.split(' ') ).map( arr => arr.filter( str => str.length != 0) )
	.map( arr => {
		let tmp = [];
		for( let i = 0; i < arr.length; i += 2 ) {
			tmp.push( `${arr[i]}_${arr[i+1]}` );	
		}
		return tmp;
	} );

let n = 0; let oldn = 0;
let iter = [ 'shiny_gold' ];
let cumul_iter = [ ];
let done = false;

while ( !done ) {
	oldn = cumul_iter.length;
	iter = input.map( arr => {
		if ( arr.slice(1).map( elt => has(iter, elt) ).reduce( (a, b) => a || b ) ) {
			return arr[0];
		} else {
			return '';
		}
	}).filter( str => str.length != 0 );
	cumul_iter = [...new Set(cumul_iter.concat( iter ))];
	n = cumul_iter.length;
	if( n == oldn ) {
		done = true;
	} else {
		oldn = n;
	}
}

console.log( n );
