
function intersect(a, b) {
	let setB = new Set(b);
	return [...new Set(a)].filter(x => setB.has(x));
}

const arr = Deno.readTextFileSync('input.txt').split('\n').map( str => Number( str ) );
arr.pop();
arr.sort();

let obj = {};

arr.forEach( (i) =>
	arr.forEach( (j) => { 
		if ( i + j < 2020 ) {
			obj[2020 - i - j] = [i,j]; 
		}
	} ) );

// Honestly, I'm not sure exactly how this worked. It got me the answer sooner than I thought it would.
let ans = intersect( arr, Object.keys(obj).map( n => Number(n)) ).reduce( 
	(a, b) => a * b );
console.log(ans);