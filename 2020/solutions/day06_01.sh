#!/bin/bash
for i in $(cat input.txt | sed -e 's/^[[:space:]]*$/,/g' | tr -d '\n' | sed -e 's/,/\n/g'); do echo $(( $( echo $i | sed -e 's/\(.\)/\1\n/g' | sort | uniq | wc -l) - 1 )); done | paste -sd+ - | bc
