
const array: string[] = Deno.readTextFileSync('input.txt').split('\n');
array.pop();
const width = array[0].length;

function count ( right: number, down: number ): number {
	let x = 0;
	let y = 0;
	let c = 0;

	while ( y < array.length ) {
		if ( array[y][x] == "#" ) {
			c += 1;
		}
		x = (x + right) % width;
		y += down;
	}

	return c;
}

console.log( [ [1,1], [3,1], [5,1], [7,1], [1,2] ].map( ( params: number[]) => count(params[0], params[1])).reduce( (a: number, b: number): number => a * b ));
	
