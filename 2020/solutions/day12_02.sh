#!/bin/bash
rm day12_02.m
printf 'input=[' >>day12_02.m
cat input.txt | sed -e 's/N/0 /g' -e 's/S/1 /g' -e 's/E/2 /g' -e 's/W/3 /g' -e 's/L/4 /g' -e 's/R/5 /g' -e 's/F/6 /g' >>day12_02.m
printf '];' >>day12_02.m
cat day12_02_aux.m >>day12_02.m
octave --no-gui day12_02.m
