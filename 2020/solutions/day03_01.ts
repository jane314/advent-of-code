
const array: string[] = Deno.readTextFileSync('input.txt').split('\n');
array.pop();
const width = array[0].length;
const down = 1;
const right = 3;

let x = 0;
let y = 0;
let count = 0;

while ( y < array.length ) {
	if ( array[y][x] == "#" ) {
		count += 1;
	}
	x = (x + right) % width;
	y += 1;
}

console.log(count);

