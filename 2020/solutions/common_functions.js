// spread and Set are awesome!!

// intersection of 2 arrays
function intersect(a, b) {
	let setB = new Set(b);
	return [...new Set(a)].filter(x => setB.has(x));
}

// sort and filter out duplicate elements of an array
function sortuniq( arr ) {
	return [...new Set( arr.sort() ) ];
}
