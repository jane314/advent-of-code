
let input = Deno.readTextFileSync('input.txt').split('\n').map( str => str.split(' ') );
input.pop();
input = input.map( arr => { return { 'ran': 0, 'cmd': arr[0], 'num': Number(arr[1]) } } );

let index = 0;
let obj = input[ index ];
let accumulator = 0;

while( obj.ran < 2 ) {
	obj = input[ index ];
	obj.ran += 1;
	if( obj.cmd == 'nop' ) {
		index += 1;
	} else if( obj.cmd == 'jmp' ) {
		index += obj.num;
	} else {
		index += 1;
		accumulator += obj.num;
	}
}

console.log( accumulator);


