let input: string = Deno.readTextFileSync('output.txt');
let array: string[][] = input.split('\n').map( str => str.split(',') );
array.pop();
let count = 0;

function find_num( chr: string, str: string ): number {
	let n = 0;
	for( let i = 0; i < str.length; i++) {
		if( str[i] == chr ) {
			n += 1;
		}
	}
	return n;
}


array.forEach( ( arr: string[] ) => {
	let num = find_num( arr[2], arr[3] );
	if( Number(arr[0]) <= num && Number(arr[1]) >= num ) {
		count += 1;
	}
} );

console.log(count);

