while read -r i
do
	row="$((2#$(echo $i | tr -d 'RL' | sed -e 's/F/0/g' -e 's/B/1/g')))"
	col="$((2#$(echo $i | tr -d 'FB' | sed -e 's/R/1/g' -e 's/L/0/g')))"
	echo "$((row * 8 + col))"
done