
// run my_program prog, mark which ones hit
// backtrace from end, recursivley, making sure can flip only once in backtrace, skip ones not hit, or try to reach one hit? ? or somethign?

/*
 * sortuniq
 * return arr with duplicates filtered out
 */
function sortuniq( arr ) {
	return [...new Set( arr.sort() ) ];
}

/*
 * run_program
 * Runs an program program
 * Returns { infinite: __ (t/f), acc: __, indices: used: [] }
 */
function run_program( program ) {
	let ret_obj = { 'infinite': false, 'acc': 0, 'indices': [] };
	let index = 0;
	let cur_loc = program[ index ];
	ret_obj.acc = 0;
	while( cur_loc.ran < 2 && index < program.length ) {
		cur_loc = program[ index ];
		ret_obj.indices.push( index );
		cur_loc.ran += 1;
		if( cur_loc.cmd == 'nop' ) {
			index += 1;
		} else if( cur_loc.cmd == 'jmp' ) {
			index += cur_loc.num;
		} else {
			index += 1;
			ret_obj.acc += cur_loc.num;
		}
	}
	if ( index < program.length ) {
		ret_obj.infinite = true;	
	}
	ret_obj.indices = sortuniq( ret_obj.indices );
	return ret_obj;
}

/*
 * backtrace_aux	
 * Find commands that could possibly lead to given index, either allowing flipping or not
 * programs: program, index 
 * Returns array of prog line objects
 */
function backtrace_aux( program_data, index ) {
	return Object.keys(program_data).map( key => {
		if ( program_data[ key ].cmd ==	'jmp' && program_data[ key ].num + program_data[ key ].index == index ) {
			return { 'flip': false, 'program_data[ key ]': program_data[ key ] };
		} else if ( program_data[ key ].cmd ==	'nop' && program_data[ key ].num + program_data[ key ].index == index ) {
			return { 'flip': true, 'program_data[ key ]': program_data[ key ] };
		} else if ( program_data[ key ].cmd ==  'nop' && program_data[ key ].index == index - 1 ) {
			return { 'flip': false, 'program_data[ key ]': program_data[ key ] };
		} else if ( ( program_data[ key ].cmd ==  'jmp' || program_data[ key ].cmd == 'acc' ) && program_data[ key ].index == index - 1 ) {
			return { 'flip': true, 'program_data[ key ]': program_data[ key ] };
		} else {
			return null;
		}
	} ).filter( x => x != null );
}

function backtrace( program_data ) {
	let iter = [ Object.keys(program_data).length + 1 ];
	let index_to_flip = -1;
	while( index_to_flip == -1 ) {
		iter = iter.map( index => backtrace_aux( program_data, index ) )
			.filter( obj => obj.flip )
			.map( obj => obj.index );
		if( iter.length != 0 ) {
			index_to_flip = iter[0];
		}
	}
	return index_to_flip;
}



/*
 * Main data structures:
 * my_program
 * 			[ 
 * 				{ index: __, ran: __, cmd: __, num: __ }
 * 			]
 * 	program_data 
 * 		{	
 * 			'0': { index: 0, ran: (not 0), cmd: __, num: __ }
 * 			'1' ...
 * 		}
 */
let my_program = Deno.readTextFileSync('input.txt').split('\n').map( str => str.split(' ') );
my_program.pop();
my_program = my_program.map( ( arr, index ) => { return { 'index': index, 'ran': 0, 'cmd': arr[0], 'num': Number(arr[1]) } } );
let program_data = {};
my_program.filter( obj => obj.ran != 0 )
	.forEach( obj => {
		program_data[ obj.index ] = obj;
	} );
console.dir( program_data );
//console.log( backtrace( my_program ) );

