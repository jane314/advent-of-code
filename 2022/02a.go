package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func RPSRound(player1, player2 byte) int {
	theirScore := int(player1) - 64
	youChoseScore := int(player2) - 87
	if theirScore == youChoseScore {
		return 3 + youChoseScore
	} else if (theirScore == 1 && youChoseScore == 2) || (theirScore == 2 && youChoseScore == 3) || (theirScore == 3 && youChoseScore == 1) {
		return 6 + youChoseScore
	} else {
		return youChoseScore
	}
}

func Prob02a() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var line string
	var sum int
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		sum += RPSRound(line[0], line[2])
	}
	fmt.Println(sum)
}
