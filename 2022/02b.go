package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func mod(x int) int {
	if x == 0 {
		return 3
	} else if x == 4 {
		return 1
	}
	return x
}

func RPSRoundReverse(player1, outcome byte) int {
	theirScore := int(player1) - 64
	if outcome == 'X' {
		// Should lose
		return mod(theirScore - 1)
	} else if outcome == 'Y' {
		// should draw
		return 3 + theirScore
	} else {
		// should win
		return 6 + mod(theirScore+1)
	}
}

func Prob02b() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var line string
	var sum int
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		sum += RPSRoundReverse(line[0], line[2])
	}
	fmt.Println(sum)
}
