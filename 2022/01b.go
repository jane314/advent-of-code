package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func Prob01b() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var cur, max1, max2, max3 int64
	var line string
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		if line == "" {
			if cur >= max3 && cur <= max2 {
				max3 = cur
			} else if cur >= max2 && cur <= max1 {
				max3 = max2
				max2 = cur
			} else if cur >= max1 {
				max3 = max2
				max2 = max1
				max1 = cur
			}

			cur = 0
		} else {
			next, _ := strconv.ParseInt(scan.Text(), 10, 64)
			cur += next
		}
	}
	fmt.Println(max1 + max2 + max3)
}
