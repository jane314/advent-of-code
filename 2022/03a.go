package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func Intersect(str string) byte {
	n := len(str) / 2
	for _, i := range []byte(str[0:n]) {
		for _, j := range []byte(str[n:]) {
			if i == j {
				return i
			}
		}
	}
	return '\x00'
}

func Byte2Num(b byte) int {
	if 'a' <= b && b <= 'z' {
		return int(b) - 96
	} else {
		return int(b) - 38
	}
}

func Prob03a() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var line string
	var sum int
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		sum += Byte2Num(Intersect(line))
	}
	fmt.Println(sum)
}
