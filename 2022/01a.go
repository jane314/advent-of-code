package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func Prob01a() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var max, cur int64
	var line string
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		if line == "" {
			if cur >= max {
				max = cur
			}
			cur = 0
		} else {
			next, _ := strconv.ParseInt(scan.Text(), 10, 64)
			cur += next
		}
	}
	fmt.Println(max)
}
