package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// Naive, horrible O(n^3) intersection
func Intersect3(str1, str2, str3 string) byte {
	for _, i := range []byte(str1) {
		for _, j := range []byte(str2) {
			if i == j {
				for _, k := range []byte(str3) {
					if i == k {
						return i
					}
				}
			}
		}
	}
	return '\x00'
}

func Prob03b() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var sum int
	/*
	 * Execution
	 */
	for scan.Scan() {
		line1 := scan.Text()
		scan.Scan()
		line2 := scan.Text()
		scan.Scan()
		line3 := scan.Text()
		sum += Byte2Num(Intersect3(line1, line2, line3))
	}
	fmt.Println(sum)
}
