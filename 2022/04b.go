package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func Prob04b() {
	/*
	 * Read file
	 */
	filePath := "input.txt"
	readFile, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	scan := bufio.NewScanner(readFile)
	scan.Split(bufio.ScanLines)
	/*
	 * Running sums
	 */
	var line string
	var count int
	/*
	 * Execution
	 */
	for scan.Scan() {
		line = scan.Text()
		pairs := strings.Split(line, ",")
		pair1 := strings.Split(pairs[0], "-")
		pair2 := strings.Split(pairs[1], "-")
		a1, _ := strconv.Atoi(pair1[0])
		a2, _ := strconv.Atoi(pair1[1])
		b1, _ := strconv.Atoi(pair2[0])
		b2, _ := strconv.Atoi(pair2[1])
		if (a1 <= b2 && b2 <= a2) || (a1 <= b1 && b1 <= a2) || (b1 <= a2 && a2 <= b2) || (b1 <= a1 && a1 <= b2) {
			count++
		}
	}
	fmt.Println(count)
}
