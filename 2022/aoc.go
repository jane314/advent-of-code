package main

import (
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) == 1 {
		log.Fatal("Need problem # in format 01a, 02b, etc.")
	} else if strings.ToLower(os.Args[1]) == "01a" {
		Prob01a()
	} else if strings.ToLower(os.Args[1]) == "01b" {
		Prob01b()
	} else if strings.ToLower(os.Args[1]) == "02a" {
		Prob02a()
	} else if strings.ToLower(os.Args[1]) == "02b" {
		Prob02b()
	} else if strings.ToLower(os.Args[1]) == "03a" {
		Prob03a()
	} else if strings.ToLower(os.Args[1]) == "03b" {
		Prob03b()
	} else if strings.ToLower(os.Args[1]) == "04a" {
		Prob04a()
	} else if strings.ToLower(os.Args[1]) == "04b" {
		Prob04b()
	} else {
		log.Fatal("This problem # is not valid: ", os.Args[1])
	}
}
